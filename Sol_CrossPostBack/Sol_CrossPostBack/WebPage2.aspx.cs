﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Sol_CrossPostBack
{
    public partial class WebPage2 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            #region Bad Practice
            //TextBox textNameobj = PreviousPage.FindControl("txtName") as TextBox;
            //spanName.InnerText = textNameobj.Text;

            //TextBox textNameobj1 = PreviousPage.FindControl("txtName1") as TextBox;
            //spanName1.InnerText = textNameobj1.Text;

            #endregion

            #region Good Practice

            spanName.InnerText = ((TextBox)PreviousPage.FindControl("txtName")).Text;

            spanName1.InnerText = ((TextBox)PreviousPage.FindControl("txtName1")).Text;

            spanName2.InnerText = ((TextBox)PreviousPage.FindControl("txtName2")).Text;

            spanName3.InnerText = ((TextBox)PreviousPage.FindControl("txtName3")).Text;

            spanName4.InnerText = ((TextBox)PreviousPage.FindControl("txtName4")).Text;

            #endregion


        }
    }
    }
